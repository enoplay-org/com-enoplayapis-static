package usecase

import "gitlab.com/enoplay/com-enoplayapis-static/domain"

type SessionTokenInteractor struct {
	SessionTokenRepository domain.SessionTokenRepository
}

func NewSessionTokenInteractor() *SessionTokenInteractor {
	return &SessionTokenInteractor{}
}

func (interactor *SessionTokenInteractor) VerifyAccess(token string) (string, error) {
	tokenClaim, err := interactor.SessionTokenRepository.VerifyAccess(token)
	if err != nil {
		return "", err
	}

	return tokenClaim.UserUID, nil
}
