package usecase

import "gitlab.com/enoplay/com-enoplayapis-static/domain"

type ImageInteractor struct {
	ImageRepository domain.ImageRepository
}

func NewImageInteractor() *ImageInteractor {
	return &ImageInteractor{}
}

func (interactor *ImageInteractor) Get(uid string) (domain.Image, error) {
	return interactor.ImageRepository.Get(uid)
}

func (interactor *ImageInteractor) Delete(uid string) error {
	return interactor.ImageRepository.Delete(uid)
}
