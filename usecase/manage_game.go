package usecase

import (
	"fmt"
	"net/url"
	"strings"

	"gitlab.com/enoplay/com-enoplayapis-static/domain"
)

type GameInteractor struct {
	GameRepository  domain.GameRepository
	ImageRepository domain.ImageRepository
}

func NewGameInteractor() *GameInteractor {
	return &GameInteractor{}
}

// Get gets a Game by its UID
func (interactor *GameInteractor) Get(uid string) (domain.Game, error) {
	return interactor.GameRepository.GetByUID(uid)
}

// GetByGID gets a Game by its game ID (GID)
func (interactor *GameInteractor) GetByGID(gid string) (domain.Game, error) {
	return interactor.GameRepository.GetByGID(gid)
}

func (interactor *GameInteractor) UpdateThumbnail(filePath string, game domain.Game) (domain.Game, error) {
	image := domain.Image{}
	image.Owner.UID = game.UID
	image.Owner.Type = domain.ImageOwnerGameThumbnail

	// Create New Image
	image, err := interactor.ImageRepository.Create(image, filePath)
	if err != nil {
		return domain.Game{}, fmt.Errorf("Error creating new game thumbnail: %v", err.Error())
	}

	// Delete Old Image
	interactor.ImageRepository.Delete(game.Media.Thumbnail.UID)

	// Update Image
	game.Media.Thumbnail.UID = image.UID
	game.Media.Thumbnail.Source = image.Source

	game, err = interactor.GameRepository.UpdateThumbnail(game)
	if err != nil {
		interactor.ImageRepository.Delete(image.UID)
		return domain.Game{}, fmt.Errorf("Error updating game thumbnail: %v", err.Error())
	}

	return game, nil
}

func (interactor *GameInteractor) UpdateImages(filePaths []string, game domain.Game) (domain.Game, error) {
	images := make(map[string]domain.Image)

	// Create New Images
	for _, path := range filePaths {
		image := domain.Image{}
		image.Owner.UID = game.UID
		image.Owner.Type = domain.ImageOwnerGameImage

		image, err := interactor.ImageRepository.Create(image, path)
		if err != nil {
			return domain.Game{}, fmt.Errorf("Error creating new game image: %v", err.Error())
		}

		images[image.UID] = image
	}

	if game.Media.Images == nil {
		game.Media.Images = make(map[string]struct {
			UID    string `json:"uid" bson:"uid"`
			Source string `json:"source" bson:"source"`
		})
	}

	// Delete Old Images
	for _, image := range game.Media.Images {
		interactor.ImageRepository.Delete(image.UID)
		delete(game.Media.Images, image.UID)
	}

	// Update Images
	for _, image := range images {
		game.Media.Images[image.UID] = struct {
			UID    string `json:"uid" bson:"uid"`
			Source string `json:"source" bson:"source"`
		}{
			image.UID,
			image.Source,
		}
	}

	game, err := interactor.GameRepository.UpdateImages(game)
	if err != nil {
		for _, image := range images {
			interactor.ImageRepository.Delete(image.UID)
		}
		return domain.Game{}, fmt.Errorf("Error updating game images: %v", err.Error())
	}

	return game, nil
}

func (interactor *GameInteractor) UpdateVideos(videos []string, game domain.Game) (domain.Game, error) {
	mediaVideos := make(map[string]struct {
		Thumbnail string `json:"thumbnail" bson:"thumbnail"`
		Source    string `json:"source" bson:"source"`
	})

	for _, video := range videos {
		videoID, err := youtubeVideoID(video)
		if err != nil {
			return domain.Game{}, err
		}
		videoThumbnail := youtubeThumbnailFromID(videoID)
		videoSrc := youtubeVideoFromID(videoID)

		mediaVideos[videoID] = struct {
			Thumbnail string `json:"thumbnail" bson:"thumbnail"`
			Source    string `json:"source" bson:"source"`
		}{
			Thumbnail: videoThumbnail,
			Source:    videoSrc,
		}
	}

	game.Media.Videos = mediaVideos

	game, err := interactor.GameRepository.UpdateVideos(game)
	if err != nil {
		return domain.Game{}, fmt.Errorf("Error updating game videos: %v", err.Error())
	}

	return game, nil
}

func youtubeVideoID(src string) (string, error) {
	url, err := url.Parse(src)
	if err != nil {
		return "", err
	}
	if strings.Contains(src, "youtube-nocookie.com/embed") {
		// e.g. youtube-nocookie.com/embed/ZYowY7tvBbY
		return url.Path[7:], nil
	} else if strings.Contains(src, "youtube.com/embed") {
		// e.g. youtube.com/embed/ZYowY7tvBbY
		return url.Path[7:], nil
	} else if strings.Contains(src, "youtu.be") || strings.Contains(src, "y2u.be") {
		// e.g. youtu.be/ZYowY7tvBbY or y2u.be/ZYowY7tvBbY
		return url.Path[1:], nil
	} else if strings.Contains(src, "youtube") {
		// e.g. youtube.com/watch?v=ZYowY7tvBbY
		return url.Query().Get("v"), nil
	}
	return "", fmt.Errorf("Invalid youtube video")
}

func youtubeVideoFromID(videoID string) string {
	return fmt.Sprintf("https://www.youtube-nocookie.com/embed/%v?autoplay=1", videoID)
}

func youtubeThumbnailFromID(videoID string) string {
	return fmt.Sprintf("https://i.ytimg.com/vi/%v/hqdefault.jpg", videoID)
}
