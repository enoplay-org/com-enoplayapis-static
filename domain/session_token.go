package domain

import "time"

type SessionTokenRepository interface {
	VerifyAccess(tokenString string) (SessionTokenClaim, error)
}

type SessionTokenClaim struct {
	UserUID     string    `json:"userUID"`
	Type        string    `json:"type"`
	DateExpired time.Time `json:"dateExpired"`
	DateIssued  time.Time `json:"dateIssued"`
	JTI         string    `json:"jti"`
}

const SessionTokenTypeAccess = "access"
