package domain

type UserRepository interface {
	Get(uid string) (User, error)
	FindByUsername(username string) (User, error)
	UpdateIcon(user User) (User, error)
	UpdateBanner(user User) (User, error)
}

type User struct {
	UID         string `json:"uid" bson:"uid,omitempty"`
	Username    string `json:"username" bson:"username"`
	Alias       string `json:"alias" bson:"alias"`
	Description string `json:"description" bson:"description"`

	Media UserMedia `json:"media,omitempty" bson:"media"`
}

type UserMedia struct {
	Icon struct {
		UID    string `json:"uid" bson:"uid"`
		Source string `json:"source" bson:"source"`
	} `json:"icon,omitempty"`
	Banner struct {
		UID    string `json:"uid" bson:"uid"`
		Source string `json:"source" bson:"source"`
	} `json:"banner,omitempty" bson:"banner"`
}
