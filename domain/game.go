package domain

type GameRepository interface {
	GetByUID(uid string) (Game, error)
	GetByGID(gid string) (Game, error)
	UpdateThumbnail(game Game) (Game, error)
	UpdateImages(game Game) (Game, error)
	UpdateVideos(game Game) (Game, error)
}

type Game struct {
	UID string `json:"uid" bson:"uid,omitempty"`
	GID string `json:"gid,omitempty" bson:"gid"`

	// Publisher
	Publisher struct {
		UID      string `json:"uid" bson:"uid"`
		Username string `json:"username" bson:"username"`
	} `json:"publisher" bson:"publisher"`

	// Media
	Media GameMedia `json:"media,omitempty" bson:"media"`
}

type GameMedia struct {
	Thumbnail struct {
		UID    string `json:"uid" bson:"uid"` // uid of Media
		Source string `json:"source" bson:"source"`
	} `json:"thumbnail,omitempty" bson:"thumbnail"`
	Images map[string]struct {
		UID    string `json:"uid" bson:"uid"`
		Source string `json:"source" bson:"source"`
	} `json:"images,omitempty" bson:"images"`
	Videos map[string]struct { // { "uid": { "thumbnail": "", "source": ""}}
		Thumbnail string `json:"thumbnail" bson:"thumbnail"`
		Source    string `json:"source" bson:"source"`
	} `json:"videos,omitempty" bson:"videos"`
}
