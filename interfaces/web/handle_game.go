package web

import (
	"fmt"
	"net/http"
	"os"

	"github.com/go-zoo/bone"

	"gitlab.com/enoplay/com-enoplayapis-static/domain"
)

type UpdateGameThumbnailResponseV0 struct {
	Game    domain.Game `json:"game,omitempty"`
	Message string      `json:"message,omitempty"`
	Success bool        `json:"success,omitempty"`
}

type UpdateGameImagesResponseV0 struct {
	Game    domain.Game `json:"game,omitempty"`
	Message string      `json:"message,omitempty"`
	Success bool        `json:"success,omitempty"`
}

type UpdateGameVideosRequestV0 struct {
	Videos []string `json:"videos"`
}

type UpdateGameVideosResponseV0 struct {
	Game    domain.Game `json:"game,omitempty"`
	Message string      `json:"message,omitempty"`
	Success bool        `json:"success,omitempty"`
}

type GameInteractor interface {
	Get(uid string) (domain.Game, error)
	GetByGID(gid string) (domain.Game, error)
	UpdateThumbnail(filePath string, game domain.Game) (domain.Game, error)
	UpdateImages(filePaths []string, game domain.Game) (domain.Game, error)
	UpdateVideos(videos []string, game domain.Game) (domain.Game, error)
}

func (handler *WebHandler) GetGameThumbnail(res http.ResponseWriter, req *http.Request) {
	gid := bone.GetValue(req, "gid")
	game, err := handler.GameInteractor.GetByGID(gid)
	if err != nil {
		handler.util.renderer.Error(res, req, http.StatusUnprocessableEntity, err.Error())
		return
	}
	res.Write(handler.util.ReadImage(game.Media.Thumbnail.Source))
}

func (handler *WebHandler) UpdateGameThumbnail(res http.ResponseWriter, req *http.Request) {
	userUID := req.Context().Value(UserUIDContextKey)
	if userUID == nil {
		handler.util.renderer.Error(res, req, http.StatusUnauthorized, "Requires authentication")
		return
	}

	gid := bone.GetValue(req, "gid")
	game, err := handler.GameInteractor.GetByGID(gid)
	if err != nil {
		handler.util.renderer.Error(res, req, http.StatusUnprocessableEntity, err.Error())
		return
	}

	if game.Publisher.UID != userUID {
		handler.util.renderer.Error(res, req, http.StatusUnauthorized, fmt.Sprintf("Unauthorized request"))
		return
	}

	file, fileDir, filePath, err := handler.util.DecodeImageFile(req, "thumbnail")
	defer file.Close()
	defer os.RemoveAll(fileDir)
	if err != nil {
		handler.util.renderer.Error(res, req, http.StatusUnprocessableEntity, fmt.Sprintf("Error parsing file: %v", err.Error()))
		return
	}

	game, err = handler.GameInteractor.UpdateThumbnail(filePath, game)
	if err != nil {
		handler.util.renderer.Error(res, req, http.StatusUnprocessableEntity, err.Error())
		return
	}

	handler.util.renderer.Render(res, req, http.StatusOK, UpdateGameThumbnailResponseV0{
		Game:    game,
		Message: "Game thumbnail updated!",
		Success: true,
	})
}

func (handler *WebHandler) UpdateGameImages(res http.ResponseWriter, req *http.Request) {
	userUID := req.Context().Value(UserUIDContextKey)
	if userUID == nil {
		handler.util.renderer.Error(res, req, http.StatusUnauthorized, "Requires authentication")
		return
	}

	gid := bone.GetValue(req, "gid")
	game, err := handler.GameInteractor.GetByGID(gid)
	if err != nil {
		handler.util.renderer.Error(res, req, http.StatusUnprocessableEntity, err.Error())
		return
	}

	if game.Publisher.UID != userUID {
		handler.util.renderer.Error(res, req, http.StatusUnauthorized, fmt.Sprintf("Unauthorized request"))
		return
	}

	files, fileDirs, filePaths, err := handler.util.DecodeMultipleImageFiles(req)
	for _, file := range files {
		defer file.Close()
	}
	for _, fileDir := range fileDirs {
		defer os.RemoveAll(fileDir)
	}
	if err != nil {
		handler.util.renderer.Error(res, req, http.StatusUnprocessableEntity, fmt.Sprintf("Error parsing file: %v", err.Error()))
		return
	}

	game, err = handler.GameInteractor.UpdateImages(filePaths, game)
	if err != nil {
		handler.util.renderer.Error(res, req, http.StatusUnprocessableEntity, err.Error())
		return
	}

	handler.util.renderer.Render(res, req, http.StatusOK, UpdateGameImagesResponseV0{
		Game:    game,
		Message: "Game media images updated!",
		Success: true,
	})
}

func (handler *WebHandler) UpdateGameVideos(res http.ResponseWriter, req *http.Request) {
	userUID := req.Context().Value(UserUIDContextKey)
	if userUID == nil {
		handler.util.renderer.Error(res, req, http.StatusUnauthorized, "Requires authentication")
		return
	}

	var body UpdateGameVideosRequestV0
	err := handler.util.DecodeRequestBody(res, req, &body)
	if err != nil {
		handler.util.renderer.Error(res, req, http.StatusBadRequest, fmt.Sprintf("Request body parse error: %v", err.Error()))
		return
	}

	gid := bone.GetValue(req, "gid")
	game, err := handler.GameInteractor.GetByGID(gid)
	if err != nil {
		handler.util.renderer.Error(res, req, http.StatusUnprocessableEntity, err.Error())
		return
	}

	if game.Publisher.UID != userUID {
		handler.util.renderer.Error(res, req, http.StatusUnauthorized, fmt.Sprintf("Unauthorized request"))
		return
	}

	game, err = handler.GameInteractor.UpdateVideos(body.Videos, game)
	if err != nil {
		handler.util.renderer.Error(res, req, http.StatusUnprocessableEntity, err.Error())
		return
	}

	handler.util.renderer.Render(res, req, http.StatusOK, UpdateGameVideosResponseV0{
		Game:    game,
		Message: "Game media images updated!",
		Success: true,
	})
}
