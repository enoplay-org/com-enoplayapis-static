package web

type SessionTokenInteractor interface {
	VerifyAccess(token string) (string, error)
}
