package web

import (
	"context"
	"fmt"
	"net/http"
	"strings"
)

// OurOwnContextKeyType to prevent potential key collisions with 3rd party libraries
type OurOwnContextKeyType string

const UserUIDContextKey OurOwnContextKeyType = "userUID"
const AccessTokenContextKey OurOwnContextKeyType = "accessSessionToken"

// EnsureAuthorization ensures a User has been authorized to access this resource
func (handler *WebHandler) EnsureAuthorization(h http.HandlerFunc) http.HandlerFunc {
	return http.HandlerFunc(func(res http.ResponseWriter, req *http.Request) {
		authHeader := strings.Split(req.Header.Get("Authorization"), " ")
		var authSchema string
		var bearerToken string
		if len(authHeader) == 2 {
			authSchema = authHeader[0]
			bearerToken = authHeader[1]
		} else {
			handler.util.renderer.Error(res, req, http.StatusUnauthorized, "Requires Authorization: Invalid Bearer Header")
			return
		}

		if authSchema != "Bearer" {
			handler.util.renderer.Error(res, req, http.StatusUnauthorized, "Requires Authorization: Invalid Bearer Header")
			return
		}

		if bearerToken == "" {
			handler.util.renderer.Error(res, req, http.StatusUnauthorized, "Requires Authorization: Invalid Bearer Header")
			return
		}

		userUID, err := handler.SessionTokenInteractor.VerifyAccess(bearerToken)
		if err != nil {
			handler.util.renderer.Error(res, req, http.StatusUnauthorized, fmt.Sprintf("Requires Authorization: Invalid Bearer Header: %v", err))
			return
		}

		userCtx := context.WithValue(req.Context(), UserUIDContextKey, userUID)
		req = req.WithContext(userCtx)

		sessionCtx := context.WithValue(req.Context(), AccessTokenContextKey, bearerToken)
		req = req.WithContext(sessionCtx)

		h.ServeHTTP(res, req)
	})
}

func (handler *WebHandler) ForceHTTPS(res http.ResponseWriter, req *http.Request, next http.HandlerFunc) {
	if req.Header.Get("X-Forwarded-Proto") == "http" {
		urlStr := fmt.Sprintf("https://%v%v", req.Host, req.RequestURI)
		http.Redirect(res, req, urlStr, http.StatusMovedPermanently)
		return
	}
	next(res, req)
}

func (handler *WebHandler) CORS(res http.ResponseWriter, req *http.Request, next http.HandlerFunc) {

	res.Header().Set("Access-Control-Allow-Credentials", "true")
	res.Header().Set("Access-Control-Allow-Methods", "GET, POST, PATCH, PUT, DELETE, OPTIONS")
	res.Header().Set("Access-Control-Allow-Headers", "Origin, Content-Type, Access-Control-Allow-Headers, Access-Control-Allow-Credentials, Authorization")

	if origin := req.Header.Get("Origin"); origin != "" {
		res.Header().Set("Access-Control-Allow-Origin", origin)
	} else {
		res.Header().Set("Access-Control-Allow-Origin", "*")
	}

	if req.Method == "OPTIONS" {
		res.Write([]byte{1})
		return
	}

	next(res, req)
}

const maxContentLength = (2 << 20) * 5 // 5 MB

func (handler *WebHandler) MaxRequestSize(res http.ResponseWriter, req *http.Request, next http.HandlerFunc) {
	if req.ContentLength > maxContentLength {
		handler.util.renderer.Error(res, req, http.StatusRequestEntityTooLarge, fmt.Sprintf("Request content length must be less than %v bytes", maxContentLength))
		return
	}

	next(res, req)
}
