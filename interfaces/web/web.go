package web

import (
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"os"

	"github.com/twinj/uuid"
)

type WebHandler struct {
	SessionTokenInteractor SessionTokenInteractor
	ImageInteractor        ImageInteractor
	UserInteractor         UserInteractor
	GameInteractor         GameInteractor
	util                   *Utility
	options                *WebHandlerOptions
}

type WebHandlerOptions struct {
	FileUploadPathImage string
	MaxFiles            int
}

type Utility struct {
	renderer            *Renderer
	fileUploadPathImage string
	maxFiles            int
}

func NewWebHandler(options WebHandlerOptions) *WebHandler {
	util := Utility{}
	util.renderer = NewRenderer(RendererOptions{
		IndentJSON: true,
	}, JSON)
	util.fileUploadPathImage = options.FileUploadPathImage
	util.maxFiles = options.MaxFiles
	webHandler := &WebHandler{}
	webHandler.util = &util
	webHandler.options = &options
	return webHandler
}

func (util *Utility) DecodeRequestBody(res http.ResponseWriter, req *http.Request, target interface{}) error {
	decoder := json.NewDecoder(req.Body)
	err := decoder.Decode(target)
	if err != nil {
		return err
	}
	return nil
}

func (util *Utility) ReadImage(url string) []byte {
	res, err := http.Get(url)
	if err != nil {
		return []byte{}
	}
	defer res.Body.Close()

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return []byte{}
	}

	return body
}

func (util *Utility) DecodeImageFile(req *http.Request, key string) (*os.File, string, string, error) {
	var filePath string
	var fileDir string
	var destFile *os.File

	const maxFileInMemory = (1 << 20) * 100 // 100K
	if err := req.ParseMultipartForm(maxFileInMemory); err != nil {
		return destFile, "", "", err
	}

	fileHeaders := req.MultipartForm.File[key]
	if fileHeaders == nil {
		var err = fmt.Errorf("%v file required", key)
		return destFile, "", "", err
	}

	var uniquePath = uuid.NewV4().String()

	for _, header := range fileHeaders {
		fileDir = fmt.Sprintf("%v/%v", util.fileUploadPathImage, uniquePath)
		filePath = fmt.Sprintf("%v/%v", fileDir, header.Filename)

		os.MkdirAll(fileDir, os.ModePerm)

		// open uploaded
		srcFile, err := header.Open()
		if err != nil {
			return destFile, fileDir, filePath, fmt.Errorf("Error opening uploaded image file: %v", err)
		}
		defer srcFile.Close()

		// validate Content-Type
		contentTypeBuffer := make([]byte, 512)
		if _, err = srcFile.Read(contentTypeBuffer); err != nil {
			return destFile, fileDir, filePath, fmt.Errorf("Error validating Content-Type of image file: %v", err)
		}

		contentType := http.DetectContentType(contentTypeBuffer)

		switch contentType {
		case "image/jpeg", "image/jpg", "image/png": // do nothing
		default:
			return destFile, fileDir, filePath, fmt.Errorf("Invalid file type. Expected jpg, jpeg, or png")
		}

		srcFile.Seek(0, 0)
		// open destination
		destFile, err = os.Create(filePath)
		if err != nil {
			return destFile, fileDir, filePath, fmt.Errorf("Error opening the destination file path for image file: %v", err)
		}

		// 32K buffer copy
		if _, err = io.Copy(destFile, srcFile); err != nil {
			return destFile, fileDir, filePath, fmt.Errorf("Error copying image file from buffer: %v", err)
		}
	}

	return destFile, fileDir, filePath, nil
}

func (util *Utility) DecodeMultipleImageFiles(req *http.Request) ([]*os.File, []string, []string, error) {
	var filePaths []string
	var fileDirs []string
	var destFiles []*os.File

	const maxFileInMemory = (1 << 20) * 200 // 200K
	if err := req.ParseMultipartForm(maxFileInMemory); err != nil {
		return destFiles, fileDirs, filePaths, err
	}

	if req.MultipartForm == nil {
		return destFiles, fileDirs, filePaths, fmt.Errorf("Error: No files uploaded")
	}

	if len(req.MultipartForm.File) > util.maxFiles {
		return destFiles, fileDirs, filePaths, fmt.Errorf("Error: Too many files uploaded")
	}

	for _, fileHeaders := range req.MultipartForm.File {
		if fileHeaders == nil {
			return destFiles, fileDirs, filePaths, fmt.Errorf("Error reading file header")
		}

		var filePath string
		var fileDir string
		var destFile *os.File
		var uniquePath = uuid.NewV4().String()

		for _, header := range fileHeaders {
			fileDir = fmt.Sprintf("%v/%v", util.fileUploadPathImage, uniquePath)
			filePath = fmt.Sprintf("%v/%v", fileDir, header.Filename)

			os.MkdirAll(fileDir, os.ModePerm)

			// open uploaded
			srcFile, err := header.Open()
			if err != nil {
				return destFiles, fileDirs, filePaths, fmt.Errorf("Error opening uploaded image file: %v", err)
			}
			defer srcFile.Close()

			// validate Content-Type
			contentTypeBuffer := make([]byte, 512)
			if _, err = srcFile.Read(contentTypeBuffer); err != nil {
				return destFiles, fileDirs, filePaths, fmt.Errorf("Error validating Content-Type of image file: %v", err)
			}

			contentType := http.DetectContentType(contentTypeBuffer)
			switch contentType {
			case "image/jpeg", "image/jpg", "image/png": // do nothing
			default:
				return destFiles, fileDirs, filePaths, fmt.Errorf("Invalid file type. Expected jpg, jpeg, or png")
			}

			srcFile.Seek(0, 0)
			// open destination
			destFile, err = os.Create(filePath)
			if err != nil {
				return destFiles, fileDirs, filePaths, fmt.Errorf("Error opening the destination file path for image file: %v", err)
			}

			// 32K buffer copy
			if _, err = io.Copy(destFile, srcFile); err != nil {
				return destFiles, fileDirs, filePaths, fmt.Errorf("Error copying image file from buffer: %v", err)
			}

			destFiles = append(destFiles, destFile)
			fileDirs = append(fileDirs, fileDir)
			filePaths = append(filePaths, filePath)
		}
	}

	return destFiles, fileDirs, filePaths, nil
}
