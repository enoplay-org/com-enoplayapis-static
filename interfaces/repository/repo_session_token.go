package repository

import (
	"crypto/rsa"
	"fmt"
	"time"

	jwt "github.com/dgrijalva/jwt-go"
	"gitlab.com/enoplay/com-enoplayapis-static/domain"
)

const SessionTokenCollection = "sessions"

type DbSessionTokenRepo struct {
	DbRepo
	TAOptions *TokenAuthorityOptions
}

type TokenAuthorityOptions struct {
	PrivateSigningKey *rsa.PrivateKey
	PublicSigningKey  *rsa.PublicKey
}

func NewDbSessionTokenRepo(dbHandlers map[string]DbHandler, taOptions *TokenAuthorityOptions) *DbSessionTokenRepo {
	dbSessionTokenRepo := &DbSessionTokenRepo{}
	dbSessionTokenRepo.dbHandlers = dbHandlers
	dbSessionTokenRepo.dbHandler = dbHandlers["DbSessionTokenRepo"]
	dbSessionTokenRepo.TAOptions = taOptions
	return dbSessionTokenRepo
}

func NewTokenAuthorityOptions(privateKey *rsa.PrivateKey, publicKey *rsa.PublicKey) *TokenAuthorityOptions {
	taOptions := &TokenAuthorityOptions{
		PrivateSigningKey: privateKey,
		PublicSigningKey:  publicKey,
	}

	return taOptions
}

func (repo *DbSessionTokenRepo) VerifyAccess(tokenString string) (domain.SessionTokenClaim, error) {
	// Ensure token is valid
	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodRSA); !ok {
			return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
		}

		return repo.TAOptions.PublicSigningKey, nil
	})
	if err != nil {
		return domain.SessionTokenClaim{}, fmt.Errorf("Error parsing access token: %v", err)
	}

	var claim domain.SessionTokenClaim

	if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
		claim.UserUID = claims["userUID"].(string)
		claim.Type = claims["type"].(string)
		claim.DateExpired = time.Unix(int64(claims["dateExpired"].(float64)), 0)
		claim.DateIssued = time.Unix(int64(claims["dateIssued"].(float64)), 0)
		claim.JTI = claims["jti"].(string)
	} else {
		return domain.SessionTokenClaim{}, fmt.Errorf("Invalid access token")
	}

	// Ensure token is access token
	if claim.Type != domain.SessionTokenTypeAccess {
		return domain.SessionTokenClaim{}, fmt.Errorf("Invalid access token")
	}

	// Ensure token has not expired
	if time.Now().Sub(claim.DateExpired).Seconds() > 0 {
		return domain.SessionTokenClaim{}, fmt.Errorf("Access token has expired")
	}

	return claim, nil
}
