package repository

import "gitlab.com/enoplay/com-enoplayapis-static/domain"

const UserCollection = "users"

type DbUserRepo DbRepo

func NewDbUserRepo(dbHandlers map[string]DbHandler) *DbUserRepo {
	dbUserRepo := &DbUserRepo{}
	dbUserRepo.dbHandlers = dbHandlers
	dbUserRepo.dbHandler = dbHandlers["DbUserRepo"]

	return dbUserRepo
}

func (repo *DbUserRepo) Get(userUID string) (domain.User, error) {
	var user domain.User
	err := repo.dbHandler.FindOne(UserCollection, Query{"uid": userUID}, &user)
	return user, err
}

func (repo *DbUserRepo) FindByUsername(username string) (domain.User, error) {
	var user domain.User
	err := repo.dbHandler.FindOne(UserCollection, Query{"username": username}, &user)
	return user, err
}

func (repo *DbUserRepo) UpdateIcon(user domain.User) (domain.User, error) {
	update := Query{
		"media.icon": user.Media.Icon,
	}

	change := Change{
		Update:    Query{"$set": update},
		ReturnNew: true,
	}

	err := repo.dbHandler.Update(UserCollection, Query{"uid": user.UID}, change, &user)
	return user, err
}

func (repo *DbUserRepo) UpdateBanner(user domain.User) (domain.User, error) {
	update := Query{
		"media.banner": user.Media.Banner,
	}

	change := Change{
		Update:    Query{"$set": update},
		ReturnNew: true,
	}

	err := repo.dbHandler.Update(UserCollection, Query{"uid": user.UID}, change, &user)
	return user, err
}
