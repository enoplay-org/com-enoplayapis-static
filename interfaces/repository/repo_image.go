package repository

import (
	"fmt"

	"gitlab.com/enoplay/com-enoplayapis-static/domain"

	mgo "gopkg.in/mgo.v2"
)

const ImageCollection = "images"

type DbImageRepo struct {
	DbRepo
	imageHandler ImageHandler
}

func NewDbImageRepo(dbHandlers map[string]DbHandler, imageHandler ImageHandler) *DbImageRepo {
	dbImageRepo := &DbImageRepo{}
	dbImageRepo.dbHandlers = dbHandlers
	dbImageRepo.dbHandler = dbHandlers["DbImageRepo"]
	dbImageRepo.imageHandler = imageHandler

	// ensure that collection has the right text index
	// refactor building collection index
	err := dbImageRepo.dbHandler.EnsureIndex(ImageCollection, mgo.Index{
		Key: []string{
			"$text:uid",
		},
		Background: true,
		Sparse:     true,
	})
	if err != nil {
		fmt.Printf("Ensure Image Index error: %v\n", err.Error())
	}

	return dbImageRepo
}

func (repo *DbImageRepo) Get(uid string) (domain.Image, error) {
	var mediaImage domain.Image
	err := repo.dbHandler.FindOne(ImageCollection, Query{"uid": uid}, &mediaImage)
	if err != nil {
		return domain.Image{}, fmt.Errorf("Error finding image by uid: %v", err)
	}
	return mediaImage, nil
}

func (repo *DbImageRepo) Create(image domain.Image, imageFilePath string) (domain.Image, error) {
	imageID, err := repo.imageHandler.Upload(imageFilePath)
	if err != nil {
		return domain.Image{}, fmt.Errorf("Error uploading image - \n\t%v", err.Error())
	}

	// Cloudinary returns a public ID of the resource. Assume uniqueness
	image.UID = imageID

	// Retrieve Image data (e.g. image source)
	imageDetails, err := repo.imageHandler.Retrieve(imageID)
	if err != nil {
		return domain.Image{}, fmt.Errorf("Error retrieving uploaded image - \n\t%v", err.Error())
	}

	image.Source = imageDetails.Source

	err = repo.dbHandler.Insert(ImageCollection, image)
	if err != nil {
		repo.Delete(image.UID)
		return domain.Image{}, fmt.Errorf("Error inserting uploaded image to database - \n\t%v", err.Error())
	}

	return image, nil
}

func (repo *DbImageRepo) Delete(uid string) error {

	// Remove from Image handling service
	err := repo.imageHandler.Remove(uid)
	if err != nil {
		return fmt.Errorf("Error removing image from service - \n\t%v", err.Error())
	}

	return repo.dbHandler.RemoveOne(ImageCollection, Query{"uid": uid})
}
